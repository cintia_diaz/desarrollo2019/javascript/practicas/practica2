let numeros=[];
sumaPositivos=0;
sumaNegativos=0;

for (let i = 0; i < 10; i++) {
	numeros[i]=Number(prompt('Introduce numero'));
}

for (let i = 0; i < numeros.length; i++) {
	let n=numeros[i];
	if (n>0) {
		sumaPositivos=sumaPositivos+n;
	} else {
		sumaNegativos=sumaNegativos+n;
	}
}

document.write("La suma de los elementos positivos es ", sumaPositivos,"<br>");
document.write("La suma de los elementos negativos es ", sumaNegativos);