let frase='Ejemplo';
frase=frase.toLowerCase();
let vocales={
	'a':0,
	'e':0,
	'i':0,
	'o':0,
	'u':0
};
let frecuenciaA=0;
let frecuenciaE=0;
let frecuenciaI=0;
let frecuenciaO=0;
let frecuenciaU=0;


for(let c=0;c<frase.length;c++){
	switch (frase[c]) {
		case 'a': 
		//case 'A':
			vocales.a++;
			break;
		case 'e':
			vocales['e']++;
			break;
		case 'i':
			vocales['i']++;
			break;
		case 'o':
			vocales['o']++;
			break;
		case 'u':
			vocales['u']++;
			break;
	}
}
frecuenciaA=vocales.a/frase.length;
frecuenciaE=vocales.e/frase.length;
frecuenciaI=vocales.i/frase.length;
frecuenciaO=vocales.o/frase.length;
frecuenciaU=vocales.u/frase.length;

document.write('La frecuencia de a es ',frecuenciaA*100,'%','<br>');
document.write('La frecuencia de e es ',frecuenciaE*100,'%','<br>');
document.write('La frecuencia de i es ',frecuenciaI*100,'%','<br>');
document.write('La frecuencia de o es ',frecuenciaO*100,'%','<br>');
document.write('La frecuencia de u es ',frecuenciaU*100,'%','<br>');