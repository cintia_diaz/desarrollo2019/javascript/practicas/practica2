let cadena='';
let cadenaMinus='';
let vocales={
	'a': 0,
	'e': 0,
	'i': 0,
	'o': 0,
	'u': 0
};

let contadorVocales=0;
let contadorConsonantes=0;

cadena=prompt('Introduce cadena');
cadenaMinus=cadena.toLowerCase();

for (let i = 0; i < cadenaMinus.length; i++) {
	switch (cadenaMinus[i]) {
		case 'a':
			vocales.a++;
			break;
		case 'e':
			vocales.e++;
			break;
		case 'i': 
			vocales.i++;
			break;
		case 'o': 
			vocales.o++;
			break;
		case 'u': 
			vocales.u++;
			break;
	}
}

contadorVocales=vocales.a + vocales.e + vocales.i + vocales.o + vocales.u;
contadorConsonantes=cadenaMinus.length-contadorVocales;
document.write(contadorVocales,'<br>');
document.write(contadorConsonantes);